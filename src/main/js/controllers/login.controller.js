(function () {
  'use strict';

  angular.module('coderuseApp.controllers')
          .controller('loginCtrl', ['$scope', '$state', 'store', 'dataService',
            function ($scope, $state, store, dataService) {
              angular.extend($scope, {
                user: {
                  id: '',
                  passwd: ''
                },
                login: function () {
                  dataService.getData('loginAction')
                          .then(function (data) {
                            var isCredentialCorrect = false;
                            angular.forEach(data, function (val) {
                              if (val.userID === $scope.user.id &&
                                      val.passwd === $scope.user.passwd) {
                                isCredentialCorrect = true;

                                // Store the creadential for future reference
                                var userStore = store.getNewStore('loggedInUser');
                                userStore.credential = val;
                                store.putStore('loggedInUser');

                                return false;
                              }
                            });
                            if (isCredentialCorrect) {
                              $state.go('main.root.dashboard');
                            }
                          });
                }
              });
            }]);
})();
(function () {
  'use strict';

  angular.module('coderuseApp')
          .config(['$translateProvider', function ($translateProvider) {
              $translateProvider.useStaticFilesLoader({
                prefix: 'i18n/locale-',
                suffix: '.json'
              });
              $translateProvider.preferredLanguage('en_US');
            }]);
})();
(function () {
  'use strict';
  
  angular.module('coderuseApp')
        .constant('serviceActionMap', {
          loginAction: {url: 'data/login.json'}
        });
})();

(function () {
  'use strict';

  var modules = [
    'ui.router',
    'ngCookies',
    'pascalprecht.translate',
    'coderuseApp.services',
    'coderuseApp.controllers'
  ];

  angular.module('coderuseApp', modules);
})();

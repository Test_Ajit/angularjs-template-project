(function() {
  'use strict';
  
  angular.module('coderuseApp')
  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

          $urlRouterProvider.otherwise('/login');

          $stateProvider

                  .state('login', {
                    url: '/login',                    
                    controller: 'loginCtrl',
                    templateUrl: 'templates/pages/login.html'
                  })

                  .state('main', {
                    url: '',
                    abstract: true,
                    controller: 'mainCtrl',
                    templateUrl: 'templates/layout/main.html'
                  })

                  .state('main.root', {
                    url: '',
                    abstract: true,
                    views: {
                      navbar: {
                        templateUrl: 'templates/layout/navbar.html',
                        controller: 'navbarCtrl'
                      },
                      tab: {
                        templateUrl: 'templates/layout/tab.html',
                        controller: 'tabCtrl'
                      },
                      sidebar: {
                        templateUrl: 'templates/layout/sidebar.html',
                        controller: 'sidebarCtrl'
                      },
                      content: {
                        templateUrl: 'templates/layout/content.html',
                        controller: 'contentCtrl'
                      }
                    }
                  })

                  .state('main.root.dashboard', {
                    url: '/dashboard',
                    controller: 'dashboardCtrl',
                    templateUrl: 'templates/pages/dashboard.html'
                  });
        }]);
})();